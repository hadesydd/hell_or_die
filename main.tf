provider "azurerm" {
  features {}
}

 
resource "azuread_application" "app-impulsa" {
  display_name = "app-impulsa"
  template_id  = "8adf8e6e-67b2-4cf2-a259-e3dc5476c621"

 
 web {
    homepage_url  = "https://appimpulsa-itsystemesidf.msappproxy.net/"
    redirect_uris = ["https://appimpulsa-itsystemesidf.msappproxy.net/"]
 }
 
  feature_tags {
    custom_single_sign_on = false
  }
 
  required_resource_access {
    resource_app_id = "00000003-0000-0000-c000-000000000000" # Microsoft Graph
 
    resource_access {
      id   = "df021288-bdef-4463-88db-98f22de89214" # User.Read.All
      type = "Role"
    }
 
    resource_access {
      id   = "b4e74841-8e56-480b-be8b-910348b18b4c" # User.ReadWrite
      type = "Scope"
    }
 }

}
 
resource "azuread_service_principal" "app-impulsa" {
  application_id = azuread_application.app-impulsa.application_id
  owners                        = ["43496a55-d3c2-45e4-9f03-b6f0a4a700f9"]
  use_existing   = true
  #preferred_single_sign_on_mode = "saml"
  login_url                     = "https://appimpulsa-itsystemesidf.msappproxy.net/"
  feature_tags {
    enterprise     = true
    gallery        = false
    #custom_single_sign_on = true
  }

provisioner "local-exec" {
        command = <<EOF
          clientId="$TF_VAR_ARM_CLIENT_ID"
          tenantId="$TF_VAR_ARM_TENANT_ID"
          clientSecret="$TF_VAR_ARM_CLIENT_SECRET"
          appid="${azuread_application.app-impulsa.object_id}"

          uritoken="https://login.microsoftonline.com/$tenantId/oauth2/v2.0/token"
 
          # Construct BodyToken
          bodyToken="client_id=$clientId&scope=https://graph.microsoft.com/.default&client_secret=$clientSecret&grant_type=client_credentials"
 
         # Get OAuth 2.0 Token
          tokenRequest=$(curl -s -X POST -d "$bodyToken" -H "Content-Type: application/x-www-form-urlencoded" "$uritoken")
          token=$(echo "$tokenRequest" | jq -r '.access_token')
          echo $token
          echo $appid
            curl --location --request PATCH "https://graph.microsoft.com/beta/applications/$appid" \
            --header "Content-Type: application/json" \
            --header "Authorization: Bearer $token" \
            --data '{
                "onPremisesPublishing": {
                    "externalAuthenticationType": "aadPreAuthentication",
                    "internalUrl": "http://appimpulsa.it-systemes.proxy:3000/",
                    "externalUrl": "https://appimpulsa-itsystemesidf.msappproxy.net/",
                    "isHttpOnlyCookieEnabled": false,
                    "isOnPremPublishingEnabled": true,
                    "isPersistentCookieEnabled": true,
                    "isSecureCookieEnabled": true,
                    "isStateSessionEnabled": true,
                    "isTranslateHostHeaderEnabled": true,
                    "isTranslateLinksInBodyEnabled": true
                }
            }'
        EOF
    }
}

 
resource "azuread_application" "api-impulsa" {
  display_name = "api-impulsa"
  template_id  = "8adf8e6e-67b2-4cf2-a259-e3dc5476c621"

 
 web {
    homepage_url  = "https://apiimpulsa-itsystemesidf.msappproxy.net/"
    redirect_uris = ["https://apiimpulsa-itsystemesidf.msappproxy.net/"]
 }
 
  feature_tags {
    custom_single_sign_on = false
  }
 
  required_resource_access {
    resource_app_id = "00000003-0000-0000-c000-000000000000" # Microsoft Graph
 
    resource_access {
      id   = "df021288-bdef-4463-88db-98f22de89214" # User.Read.All
      type = "Role"
    }
 
    resource_access {
      id   = "b4e74841-8e56-480b-be8b-910348b18b4c" # User.ReadWrite
      type = "Scope"
    }
 }

}
 
resource "azuread_service_principal" "api-impulsa" {
  application_id = azuread_application.api-impulsa.application_id
  owners                        = ["43496a55-d3c2-45e4-9f03-b6f0a4a700f9"]
  use_existing   = true
  #preferred_single_sign_on_mode = "saml"
  login_url                     = "https://apiimpulsa-itsystemesidf.msappproxy.net/"
  feature_tags {
    enterprise     = true
    gallery        = false
    #custom_single_sign_on = true
  }

provisioner "local-exec" {
        command = <<EOF
          clientId="$TF_VAR_ARM_CLIENT_ID"
          tenantId="$TF_VAR_ARM_TENANT_ID"
          clientSecret="$TF_VAR_ARM_CLIENT_SECRET"
          appid2="${azuread_application.api-impulsa.object_id}"

          uritoken="https://login.microsoftonline.com/$tenantId/oauth2/v2.0/token"
 
          # Construct BodyToken
          bodyToken="client_id=$clientId&scope=https://graph.microsoft.com/.default&client_secret=$clientSecret&grant_type=client_credentials"
 
         # Get OAuth 2.0 Token
          tokenRequest2=$(curl -s -X POST -d "$bodyToken" -H "Content-Type: application/x-www-form-urlencoded" "$uritoken")
          token2=$(echo "$tokenRequest2" | jq -r '.access_token')
          echo $token2
          echo $appid2
            curl --location --request PATCH "https://graph.microsoft.com/beta/applications/$appid2" \
            --header "Content-Type: application/json" \
            --header "Authorization: Bearer $token2" \
            --data '{
                "onPremisesPublishing": {
                    "externalAuthenticationType": "aadPreAuthentication",
                    "internalUrl": "http://apiimpulsa.it-systemes.proxy/",
                    "externalUrl": "https://apiimpulsa-itsystemesidf.msappproxy.net/",
                    "isHttpOnlyCookieEnabled": false,
                    "isOnPremPublishingEnabled": true,
                    "isPersistentCookieEnabled": true,
                    "isSecureCookieEnabled": true,
                    "isStateSessionEnabled": true,
                    "isTranslateHostHeaderEnabled": true,
                    "isTranslateLinksInBodyEnabled": true
                }
            }'
        EOF
    }
}